//本脚本示例如何导出当前模型的表名到指定文件
var
  I, J: Integer;
  md: TCtDataModelGraph;
  tb: TCtMetaTable;
  fn: String;
begin
  fn := OpenFileDialog('Export Table-names', 'Text files(*.txt)|*.txt', '', False);
  if fn='' then
    Exit;
  if ExtractFileExt(fn)='' then
    fn := ChangeFileExt(fn, '.txt');
  //输出当前表名
  if CurTable<>nil then
      CurOut.Add('Current table: '+CurTable.Name);
  for I:=0 to AllModels.Count-1 do //遍历所有模型图
  if AllModels.Items[I] = AllModels.CurDataModel then //只遍历当前模型图
  begin
    md := AllModels.Items[I];
    
    for J:=0 to md.Tables.Count-1 do //遍历所有表
    begin
      tb := md.Tables.Items[J];
      CurOut.Add(tb.Name);
    end;
  end;
  CurOut.SaveToFile(fn);
  if Application.MessageBox(fn+#13#10'File exported successfully! Open location folder?',
    'Export Table-names', MB_OKCANCEL or MB_ICONINFORMATION)=IDOK then
    ShellOpen('explorer', '/select, "' + fn + '"', '');
end.