object Form1: TForm1
  Left = 342
  Top = 122
  Width = 454
  Height = 479
  Caption = '测试窗体'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label23: TLabel
    Left = 24
    Top = 8
    Width = 119
    Height = 13
    Caption = '这是一个测试用的DFM'
  end
  object btnOK: TButton
    Left = 216
    Top = 400
    Width = 108
    Height = 23
    Caption = '确定'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 331
    Top = 400
    Width = 84
    Height = 23
    Cancel = True
    Caption = '取消'
    ModalResult = 2
    TabOrder = 1
  end
  object PageControl1: TPageControl
    Left = 17
    Top = 33
    Width = 404
    Height = 361
    ActivePage = TabSheet2
    TabOrder = 2
    object TabSheet2: TTabSheet
      Caption = '标签1'
      ImageIndex = 1
      object Bevel1: TBevel
        Left = 87
        Top = 17
        Width = 276
        Height = 3
        Shape = bsTopLine
      end
      object Label12: TLabel
        Left = 16
        Top = 36
        Width = 61
        Height = 13
        Caption = 'Background:'
      end
      object Label18: TLabel
        Left = 9
        Top = 12
        Width = 27
        Height = 13
        Caption = 'Table'
      end
      object Label1: TLabel
        Left = 16
        Top = 95
        Width = 31
        Height = 13
        Caption = 'Width:'
      end
      object Label2: TLabel
        Left = 17
        Top = 120
        Width = 34
        Height = 13
        Caption = 'Height:'
      end
      object Label6: TLabel
        Left = 10
        Top = 70
        Width = 20
        Height = 13
        Caption = 'Size'
      end
      object Bevel7: TBevel
        Left = 87
        Top = 74
        Width = 276
        Height = 2
        Shape = bsTopLine
      end
      object Bevel8: TBevel
        Left = 87
        Top = 152
        Width = 276
        Height = 2
        Shape = bsTopLine
      end
      object Label7: TLabel
        Left = 10
        Top = 148
        Width = 31
        Height = 13
        Caption = 'Others'
      end
      object Label10: TLabel
        Left = 10
        Top = 187
        Width = 46
        Height = 13
        Caption = 'Database'
      end
      object Bevel9: TBevel
        Left = 87
        Top = 191
        Width = 276
        Height = 2
        Shape = bsTopLine
      end
      object Label8: TLabel
        Left = 21
        Top = 212
        Width = 27
        Height = 13
        Caption = 'Type:'
      end
      object clbFill: TColorBox
        Left = 90
        Top = 31
        Width = 227
        Height = 22
        DefaultColorColor = clWhite
        NoneColorColor = clWhite
        Selected = clWhite
        ItemHeight = 16
        TabOrder = 0
      end
      object btnFill: TButton
        Left = 323
        Top = 31
        Width = 18
        Height = 24
        Caption = '..'
        TabOrder = 1
      end
      object edtWorkspaceWidth: TEdit
        Left = 89
        Top = 90
        Width = 227
        Height = 21
        TabOrder = 2
      end
      object edtWorkspaceHeight: TEdit
        Left = 89
        Top = 116
        Width = 227
        Height = 21
        TabOrder = 3
      end
      object chlShowFieldType: TCheckBox
        Left = 222
        Top = 168
        Width = 127
        Height = 18
        Caption = 'Show field type'
        TabOrder = 4
      end
      object chlShowFieldIcon: TCheckBox
        Left = 90
        Top = 167
        Width = 127
        Height = 18
        Caption = 'Show field icon'
        TabOrder = 5
      end
      object combDbEngine: TComboBox
        Left = 94
        Top = 208
        Width = 227
        Height = 22
        Style = csOwnerDrawFixed
        ItemHeight = 16
        TabOrder = 6
        Items.Strings = (
          'ORACLE'
          'MYSQL'
          'SQLSERVER'
          'STANDARD')
      end
    end
    object TabSheet1: TTabSheet
      Caption = '标签2'
      ImageIndex = 1
    end
  end
end