<%
/*
Note: The contents of this file will be directly overwritten when EZDML is upgraded. Instead of modifying this file directly, copy the file to a new name and modify the new file.

注意：程序升级时会直接覆盖此文件内容，请不要直接修改此文件，如果需要请将此文件复制到新名称，然后对新文件进行修改。

2021-12-25: 第一版发布

*/

//本页面采用EZDML模板语法（<%开头的脚本编译前会自动进行转换，类似JSP，脚本调试器里点编译能看到转换后的代码）
//生成的结果内容中也包含JS，所以要注意分清哪些代码是EZDML执行、哪些是浏览器执行的

//VUEForm.vue
//参考文档：https://element.eleme.cn/#/zh-CN

#include "VueElementLib.js_"

//main函数主程序开始

var tb=curTable; //记录当前表和字段列表
if(!tb)
  Abort();
var fds=tb.metaFields;

//下面开始输出vue文件
%><template>
  <!-- EZDML_PREVIEW_HTML:ElementVuePreview.html -->
  <el-dialog
    :title="formTitle"
    :visible="true"
    width="800px">

    <el-form
      :model="formData"
      label-width="80px">
<%
  //遍历所有字段
  for(var k=0; k<fds.count; k++) 
  { //字段循环开始
    var fd = fds.getItem(k); //取第k个字段
    if(!fd.canDisplay('sheet')) //判断字段可否在表单显示
      continue;
%>
          <el-form-item
            label="${fd.getLabelText()}"<%
    if (fd.isRequired())
    {
%>
            required<%
    }%> > <% 
    writeFieldFormItem(fd); //表单项比较复杂，改用脚本输出 
%>
          </el-form-item>
<%
  }
%>
    </el-form>

    <span slot="footer" class="dialog-footer">
      <el-button type="primary">确 定</el-button>
      <el-button >取 消</el-button>
    </span>
  </el-dialog>
</template> 

<script>
var demoFormData=${JSON.stringify(getDemoJson(tb,0,'[LABELTEXT]'),null,2)};

var vueAppInfo={
  components: {},
  props: [],
  data() {
    return {
      formTitle: '${tb.UIDisplayName}',
      formData: demoFormData
    }
  },
  computed: {},
  watch: {},
  created() {},
  mounted() {},
  methods: {    
  }
};

export default vueAppInfo;
</script>

<style>
</style>