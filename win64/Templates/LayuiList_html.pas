(*
Note: The contents of this file will be directly overwritten when EZDML is upgraded. Instead of modifying this file directly, copy the file to a new name and modify the new file.

注意：程序升级时会直接覆盖此文件内容，请不要直接修改此文件，如果需要请将此文件复制到新名称，然后对新文件进行修改。

2021-12-25: 第一版发布

*)

{$I layuiAdmin/inc/list._ps}

var
  pth: string;
  L: Integer;
begin
  pth := GetEnv('APPFOLDER');
  L:=Length(pth);
  if (Copy(pth, L, 1)<>'/') and (Copy(pth, L, 1)<>'\') then
    pth:=pth+'/';
  if Pos('\', pth)>0 then
    pth := 'file:///'+StringReplace(pth, '\','/', [rfReplaceAll]);
  pth:=pth+'Templates/layuiAdmin/';

  genLayList(CurTable,pth);
end.