(*[SettingsPanel]
Control="Label";Caption="Can use 4 kinds of specifiers: % (name), %s (name), {1} {2} {3} {4} {5} (name, display name, type, length, comment), {#} line number";Params="[FULLWIDTH]"
Control="ComboBox";Name="Format";Caption="Enter Format:";Value="SQL.Add('%');";Items="SQL.Add('%');,protected {3} {1}; //{2}";Params="[FULLWIDTH]"
[/SettingsPanel]*)

var
  I: Integer;
  S, V, T, LN: string;
begin
  V:=GetParamValue('Format');
  //alert(V);
  if Trim(V)='' then
  begin
    CurOut.Add('Please enter a format from settings.');
    Exit;
  end;
  V:=StringReplace(V,'#9',#9,[rfReplaceAll]);
  V:=StringReplace(V,'#13',#13,[rfReplaceAll]);
  V:=StringReplace(V,'#10',#10,[rfReplaceAll]);
  V:=StringReplace(V,'#35','#',[rfReplaceAll]);
  
  with CurTable do
  begin
    for I := 0 to MetaFields.Count - 1 do
    begin
      LN := V;
      if (Pos('{1}',V)>0) or (Pos('{2}',V)>0) or (Pos('{3}',V)>0) or (Pos('{4}',V)>0) or (Pos('{5}',V)>0) then
      begin
        S := StringReplace(V,'{1}',MetaFields[I].Name,[rfReplaceAll]);
        T:=MetaFields[I].DisplayName;
        if T='' then
          T := MetaFields[I].Name;
        S := StringReplace(S,'{2}',T,[rfReplaceAll]);
        T := MetaFields[I].GetLogicDataTypeName;
        S := StringReplace(S,'{3}',T,[rfReplaceAll]);
        T := IntToStr(MetaFields[I].DataLength);
        S := StringReplace(S,'{4}',T,[rfReplaceAll]);
        T := MetaFields[I].Memo;
        S := StringReplace(S,'{5}',T,[rfReplaceAll]);
        LN := S;
      end
      else if (Pos('%s', V)>0) or (Pos('%0:s', V)>0) then
        LN := Format(V,[MetaFields[I].Name])
      else if Pos('%', V)>0 then
        LN := StringReplace(V,'%',MetaFields[I].Name,[rfReplaceAll]);
      if Pos('{#}', LN)>0 then
        LN := StringReplace(LN,'{#}',IntToStr(I+1),[rfReplaceAll]);
      CurOut.Add(LN);
    end;
  end;
end.
