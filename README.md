# ezdml_green

#### 介绍
EZDML绿色版

方便没有条件安装的同学，直接GIT克隆或下载ZIP就可以直接使用。

- 这里仅提供WINDOWS版（LINUX和MAC本身官方发布的就是绿色版）。
- Template中的模板子文件夹文件太多，我压缩成ZIP了，如果需要的自己再解压一下。

#### 代码

需要代码编译的请点这里：
https://gitee.com/huzgd/ezdml