//本脚本示例如何给所有表名加前缀
//Java Script

var prfInput=prompt('Table name prefix:'); //输入前缀
if(!prfInput)
  AbortOut();
if(!confirm('Warning: Adding table name prefix: '+prfInput+'. All tables will be changed!!! Make sure you have backup everything. Are you sure to continue?'))
  AbortOut();

function checkPrefix(aName){ //检查名称是否已经有前缀，没有就加上
  if(!aName)
    return aName;
  var lName=aName.toLowerCase();
  if(lName.indexOf(prfInput.toLowerCase())==0)
    return aName;
  return prfInput+aName;
}

for(var i=0; i<allModels.count; i++) //遍历模型
//if(allModels.getItem(i) == allModels.curDataModel)
{
  var md=allModels.getItem(i);
  curOut.add('Model'+(i+1)+': '+md.nameCaption);

  for(var j=0; j<md.tables.count; j++) //遍历模型中的表
  //if(md.tables.getItem(j).isSelected)
  //if(curTable && md.tables.getItem(j).name == curTable.name)
  {
    var tb = md.tables.getItem(j);
    curOut.add('  '+(j+1)+'. '+tb.nameCaption+': checking...');

    var oName=tb.name;
    tb.name=checkPrefix(tb.name); //检查表名
    if(oName!=tb.name)
      curOut.add('    table-name '+oName+' changed to '+tb.name);

    var oName=tb.physicalName;
    tb.physicalName=checkPrefix(tb.physicalName); //检查物理表名
    if(oName!=tb.physicalName)
      curOut.add('    table-physicalName '+oName+' changed to '+tb.physicalName);

    for(var k=0; k<tb.metaFields.count; k++) //遍历表字段
    {
      var fd = tb.metaFields.getItem(k);
      if(fd.relateTable){ 
        var oName=fd.relateTable;
        fd.relateTable= checkPrefix(fd.relateTable); //检查外键或关联表名
        if(oName!=fd.relateTable)
          curOut.add('    Field'+(k+1)+': '+fd.nameCaption+' relate-table '+oName+' changed to '+fd.relateTable);
      }
    }
  }
  //release memory
  //JS占内存高，必要时要手工释放内存
  _gc();
}

var act=FindChildComp(application.mainForm,'actRefresh');//刷新
if(act)
  act.execute();

var txt=curOut.text;
InputMemoQuery('Table prefix', 'Result',txt,true); //输出结果提示
