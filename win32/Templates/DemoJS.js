/*(*[SettingsPanel]
Control="Label";Caption="This is a demo of javascript.";Params="[FULLWIDTH]"
Control="Edit";Name="Author";Caption="Your name";";Value="huz";Params="[FULLWIDTH]"
Control="Edit";Name="IsTest";Caption="Test flag";";Value="1";Params="[HIDDEN]"
Control="ComboBox";Name="GenType";Caption="Generate type";Items="Type1,Type2,Type3,Yes#44Is Type4";Value="Type1"
Control="RadioBox";Name="Param1";Caption="Param1";Items="V1,V2,V3";Value="V1"
Control="CheckBox";Name="Param2";Caption="Param2";Items="V1";Value="V2"
Control="Button";Name="Help";Caption="Click here for help";";Value="Help..."
[/SettingsPanel]*)*/

if (curSettingsPanel && curSettingsPanel.curAction=="Help")
{ 
  var s=curSettingsPanel.getItemValue('Author');
  alert(s+ ' '+curSettingsPanel.getItemValue('IsTest'));
  curSettingsPanel.setItemValue('Author',s+'A');
  curSettingsPanel.setItemValue('IsTest',s+'B');
  curOut.add("Button 'Help' is clicked");
  _abort();
}
var dt1=new Date();
for (var i=0; i<allModels.count; i++)
{
	var md=allModels.getItem(i);
	curOut.add("model"+i+": "+md.name);
	for (var j=0; j<md.tables.count; j++)
	{
		var tb=md.tables.getItem(j);
		curOut.add('  table'+j+': '+tb.name+"（"+tb.caption+"） date:"+( tb.createDate>0?_dateTimeToStr(tb.createDate):" none"));

		var fds=tb.metaFields;
		for (var k=0; k<fds.count; k++)
		{
			var fd=fds.getItem(k);
			curOut.add('    field'+k+': '+fd.name);
		}
	}
	break;
}
var dt2=new Date();
curOut.add('time: '+(dt2.getTime()-dt1.getTime()));
