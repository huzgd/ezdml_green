#coding=utf-8

#Note: The contents of this file will be directly overwritten when EZDML is upgraded. Instead of modifying this file directly, copy the file to a new name and modify the new file.
#注意：程序升级时会直接覆盖此文件内容，请不要直接修改此文件，如果需要请将此文件复制到新名称，然后对新文件进行修改。

import json
import sys
import time
import os

c=len(sys.argv)
fi=os.getenv('EZDML_PY_JSON_FILE_IN','')
fo=os.getenv('EZDML_PY_RES_FILE_OUT','')
if fi!='':
    print('read env variables: EZDML_PY_JSON_FILE_IN & EZDML_PY_RES_FILE_OUT')
else:
    if c<3: #需要两个参数，或者环境变量
        print('Usage: python pytest.py input_dml.json output_file.txt')
        print('       or set to env variable:')
        print('       EZDML_PY_JSON_FILE_IN')
        print('       EZDML_PY_RES_FILE_OUT')
        sys.exit(0)
    
    fi=sys.argv[1] #输入文件名
    fo=sys.argv[2] #输出文件名

#字段类型定义
dataTypeName=['Unknow','String','Integer','Float','Date','Bool','Enum','Blob','Object','Calculate','List','Function','Event','Other']

print("input file: "+fi)
dm={}
f = open(fi,"r",encoding='utf-8')  #打开JSON文件读取内容
data = f.read()
f.close()
dm=json.loads(data)

'''
{
  "RootName": "TCtMetaTable",
  "CTVER": "43543330",
  "Name": "member",
  "Caption": "会员",
  "MetaFields": {
    "Count": 32,
    "items": [
      {
        "ID": 26,
        "Name": "id",
        "DisplayName": "ID",
        "DataType": 2,
        "KeyFieldType": 1,
        "RelateTable": "users",
        "RelateField": "id",
        "Not_Nullable": true,
      },
    ]
  }
}
'''

#输出测试信息
tout=['以下内容由python生成：ezdml py ' + time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())]

#输出表名和字段列表
tout.append('表名：'+dm['Name']+' '+dm.get('Caption','')) 
mfs=dm['MetaFields'];
fs=mfs['items'];
i=1;
for f in fs:
    tout.append(' 字段'+str(i)+': '+f['Name']+' '+f.get('DisplayName','')+' 类型: '+dataTypeName[f['DataType']]);
    i=i+1
data="\n".join(tout)

print("output file: "+fo) #将结果写到文本文件
f = open(fo,"w",encoding='utf-8')
f.write(data)
f.close()

print("done!")

