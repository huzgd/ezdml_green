(*[SettingsPanel]
Control="Edit";Name="FromUser";Caption="From user:";Value="userFrom";Params=""
Control="Edit";Name="ToUser";Caption="To user:";Value="userTo";Params=""
Control="ComboBox";Name="SqlType";Caption="Sql type:";Value="Create";Items="All,Drop,Grant,Create"
[/SettingsPanel]*)

var
  N, I, po: Integer;
  U1, U2, S, fdN, tp, tps, tpv: string;
begin
  U1:=GetParamValue('FromUser');
  U2:=GetParamValue('ToUser');
  tp:=GetParamValue('SqlType');
  if Trim(U1)='' then
  begin
    CurOut.Add('From-user not specified.');
    Exit;
  end;
  if Trim(U2)='' then
  begin
    CurOut.Add('To-user not specified.');
    Exit;
  end;

  tps := 'Drop,Grant,Create';

  with CurTable do
  begin
    for N:=1 to 3 do
    begin
      po := Pos(',', tps);
      if po>0 then
      begin
        tpv:=Copy(tps,1,po-1);
        tps:=Copy(tps,po+1,Length(tps));
      end
      else
      begin
        tpv := tps;
        tps :='';
      end;

      if (tpv=tp) or (tp='All') then
      begin
        for I := 0 to MetaFields.Count - 1 do
        begin
          fdN:=MetaFields[I].Name;
          S:='';
          if (N=1) or (N=0) then
          begin
            if Pos('SEQ_', UpperCase(fdN))=1 then
              S:=Format('drop sequence %s.%s;',[U2, fdN])
            else if Pos('PKG_', UpperCase(fdN))=1 then
              S:=Format('drop package %s.%s;',[U2, fdN])
            else if Pos('FUN_', UpperCase(fdN))=1 then
              S:=Format('drop function %s.%s;',[U2, fdN])
            else if Pos('PROC_', UpperCase(fdN))=1 then
              S:=Format('drop procedure %s.%s;',[U2, fdN])
            else
              S:=Format('drop table %s.%s;',[U2, fdN]);
          end;
          if (N=2) or (N=0) then
            S:=Format('grant all on %s.%s to %s;',[U1,fdN,U2]);
          if (N=3) or (N=0) then
            S:=Format('create or replace synonym %s.%s for %s.%s;',[U2,fdN,U1,fdN]);
          if S<>'' then
            CurOut.Add(S);
        end;
        CurOut.Add('');
      end;
    end;
  end;
end.
