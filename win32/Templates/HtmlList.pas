<%
var
  I, J: Integer;
  S,T, tbN, Tmp1,Tmp2,Tmp3: String;
  F: TCTMetaField;
  col, listColCount:Integer;
begin
  tbN:=CurTable.UIDisplayName;
%>
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
      ${tbN}列表
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
      TABLE { FONT-SIZE: 12px; COLOR: #000000 } .TitleLine { padding-left: 12px; FONT-WEIGHT:
      bold; FONT-SIZE: 12px; COLOR: #ffffff; background-color: #578b43; height:
      28px; padding-top:6px; } .TitleLine2 { padding-left: 12px; FONT-WEIGHT:
      bold; FONT-SIZE: 12px; COLOR: #ffffff; background-color: #578b43; height:
      20px; padding-top:4px; } .Titleline2_bg{ background-color:#f3f3ec; border-bottom:2px
      solid #578b43; padding:10px; } .Titleline2td{ height:18px; } .vt9_title
      { font-weight: bold; font-size: 12px; color: #2F2F2F; background-color:
      #CCCCCC; height: 20px; line-height: 24px; } .T9_blackB { FONT-WEIGHT: bold;
      FONT-SIZE: 12px; COLOR: #008000; LINE-height: 25px; TEXT-DECORATION: none;
      TEXT-align: left; background-color:#cecece; padding-left:5px; }
      .table_border td{border-top:1px #888 solid;border-right:1px #888 solid;padding-left:5px;}
      .table_border{border-bottom:1px #888 solid;border-left:1px #888 solid;}
    </style>
  </head>

<body>

<table cellSpacing="1" cellPadding="0" width="660" border="0" id="table7">
  <tr>
  <td class="TitleLine" colSpan="4" >
    ${tbN}
  </td>
  </tr>
  <tr>
  <td bgColor="#ffffff" colSpan="4" height="1">
  </td>
  </tr>
  <tr class="vt9_title">
  <td noWrap width="100%" colSpan="4" >
    &nbsp; ${tbN}信息查询
  </td>
  </tr>
  <tr>
  <td colSpan="4" height="5">
  </td>
  </tr>
  <tr height="1"><td width="100"></td><td width="230"><td width="100"></td><td width="230"></td></tr>


<%
  col := 1;
  listColCount:=0;
  for I:=0 to CurTable.MetaFields.Count-1 do
  begin
    F:=CurTable.MetaFields.Items[I];
    S:=F.Name;
    if (F.KeyFieldType=cfktId) or (F.KeyFieldType=cfktRid) then
      Continue;
    if F.DisplayName<>'' then
      S:=F.DisplayName;
    listColCount:=listColCount+1;
    if(Pos('内容',S)>0) or (Pos('说明',S)>0) or (Pos('备注',S)>0) or (Pos('列表',S)>0)
      or (F.DataLength>=9999) or (F.DataType=cfdtBlob) then
    begin
      Continue;
    end;

    if col=3 then
    begin
      CurOut.Add('</tr>');
      col := 1;
    end;
    if col=1 then
    begin
      CurOut.Add('');
      CurOut.Add('<tr>');
    end;
    CurOut.Add('<td>'+S+'</td>');
    if (F.DataType = cfdtBool) then
      T:='<td><input type="checkbox"/>是</td>'
    else if(Pos('类型',S)>0) or (Pos('类别',S)>0) or (Pos('所属',S)>0) or (Pos('状态',S)>0) or (Pos('级别',S)>0) or (Pos('选择',S)>0)
      or (Pos('时间',S)>0) or (Pos('日期',S)>0) or (F.DataType = cfdtDate) or (F.DataType=cfdtEnum) then
    begin
      if (F.DataType = cfdtInteger) or (F.DataType = cfdtFloat) or (F.DataType = cfdtDate) then
        T:='<td><select style="WIDTH: 90px"></select>-<select style="WIDTH: 90px"></select></td>'
      else
        T:='<td><select style="WIDTH: 186px"></select></td>';
    end
    else
    begin
      if (F.DataType = cfdtInteger) or (F.DataType = cfdtFloat) or (F.DataType = cfdtDate) then
        T:='<td><input style="WIDTH: 90px" />-<input style="WIDTH: 90px" /></td>'
      else
        T:='<td><input style="WIDTH: 186px" /></td>';
    end;
    CurOut.Add(T);
    col:=col+1;
  end;
  if col=2 then
  begin
    CurOut.Add('<td></td><td></td>');
    col:=3;
  end;
  if col=3 then
  begin
    CurOut.Add('</tr>');
  end;
  CurOut.Add('');

%>

<tr>
<td height="6" colSpan="4" >
</td>
</tr>
<tr>
<td></td>
<td colspan="3">
    <input type="button" value="　　查　询　　">
    &nbsp;&nbsp; 提示：
      <span id="Cabp_Control_errtip" style="COLOR: red">
      请输入过滤条件进行查询。
      </span>
</td></tr>
</table>


<table cellSpacing="1" cellPadding="0" border="0" id="table8">
<tr>
<td height="6" colSpan="4" >
</td>
</tr>
<tr>
<tr>
<td height="2" class="T9_blackB" colSpan="4" >
</td>
</tr>
<tr>
<td height="6" colSpan="4" >
</td>
</tr>

<tr>
<tr>
<td colSpan="4" >
<div>
<table class="table_border" border="0" cellspacing="0" cellpadding="0" width="${IntToStr(listColCount*120+30)}">


<%
  for J:=0 to 10 do
  begin
    if J=0 then
      CurOut.Add('<tr height="25" class="T9_blackB">')
    else
      CurOut.Add('<tr height="25">');
    col := 1;
    for I:=0 to CurTable.MetaFields.Count-1 do
    begin
      F:=CurTable.MetaFields.Items[I];
      if (F.KeyFieldType=cfktId) or (F.KeyFieldType=cfktRid) then
        Continue;           
      S:=F.Name;
      if F.DisplayName<>'' then
        S:=F.DisplayName;

      if J>0 then
      begin
        S := F.GenDemoData(J, '', nil);
      end;
      if col=1 then
        CurOut.Add('<td width="150">'+S+'</td>')
      else
        CurOut.Add('<td width="120">'+S+'</td>');
      col:=col+1;
    end;
    CurOut.Add('</tr>');
  end;
%>

</table>
</div>
</td>
</tr>

<tr>
</table>

</body>

</html>

<%
end.
%>
