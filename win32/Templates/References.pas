var
  I, J, K, r1,r2: Integer;
  md: TCtDataModelGraph;
  tb: TCtMetaTable;
  fd: TCtMetaField;
  tbN: String;
begin
  if CurTable=nil then
  begin
    CurOut.Add('No table selected');
    Exit;
  end;
  tb := CurTable;
  tbN:=tb.Name;
  CurOut.Add('References of '+tb.NameCaption+':');

  r1 := 0;
  for K:=0 to tb.MetaFields.Count -1 do
  begin
    fd := tb.MetaFields.Items[K];
    if (fd.RelateTable<>'') and (fd.RelateField<>'') then
    begin
      r1 := r1+1;
      CurOut.Add(fd.RelateTable + '.'+fd.RelateField+#9#9#9'(by field '+fd.Name+')');
    end;
  end;
  CurOut.Add(IntToStr(r1)+' tables.');

  CurOut.Add('');
  CurOut.Add('Referenced by:');
  r2:=0;
  for I:=0 to AllModels.Count-1 do
  begin
    md := AllModels.Items[I];
    for J:=0 to md.Tables.Count-1 do
    begin
      tb := md.Tables.Items[J];
      for K:=0 to tb.MetaFields.Count -1 do
      begin
        fd := tb.MetaFields.Items[K];
        if (UpperCase(fd.RelateTable)=UpperCase(tbN)) and (fd.RelateField<>'') then
        begin
          r2 := r2+1;
          CurOut.Add(tb.Name + '.'+fd.Name+#9#9#9'(to field '+fd.RelateField+')');
        end;
        //CurOut.Add('    Field'+IntToStr(K)+': '+fd.NameCaption);
      end;
    end;
  end;
  CurOut.Add(IntToStr(r2)+' tables.');
end.
