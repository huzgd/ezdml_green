//本脚本演示如何直接用JS脚本调用python（需要Template目录下的pytest1.py配合）
/*
Note: The contents of this file will be directly overwritten when EZDML is upgraded. Instead of modifying this file directly, copy the file to a new name and modify the new file.
注意：程序升级时会直接覆盖此文件内容，请不要直接修改此文件，如果需要请将此文件复制到新名称，然后对新文件进行修改。
*/

if(!curTable){
  curOut.Add('请选择一个表对象');
  AbortOut();
}

var fJson=GetAppTempFileName("pyInput.json");//保存JSON临时文件名，传给python的，注意扩展名必须是json
curTable.saveToFile(fJson);  //保存JSON
SetEnvVar('EZDML_PY_JSON_FILE_IN', fJson); //可选择通过环境变量或命令行参数传给python

var fOut=GetAppTempFileName("pyResOut.txt"); //结果文件名，从python传回结果用的
if(FileExists(fOut)) //删除旧的临时文件
  DeleteFile(fOut);
fOut=GetUnusedFileName(fOut);//确保结果文件名不存在（删除可能会失败的）
SetEnvVar('EZDML_PY_RES_FILE_OUT', fOut); //通过环境变量或命令行参数传给python

//取得python文件
var pyfn=GetEnv('SCRIPTFOLDER')+'\\pytest1.py';
//运行python命令，传入输入输出两个文件名参数，生成结果
var cmd='python '+pyfn+' '+fJson+' '+fOut;
//var cmd='python '+pyfn; //如py脚本能读取环境变量获取输入输出文件名，则不需要传参
curOut.add('运行命令:\n'+cmd);
var ro=RunCmd(cmd, -3);
curOut.add('\n控制台输出:\n'+ro);

curOut.add('读取结果文件内容:');
var res=ReadFile(fOut); //读回来
curOut.add(res);  //输出
