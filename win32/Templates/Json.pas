(*[SettingsPanel]
Control="Memo";Name="memJson";Caption="Json input:";Value="{#13#10 age:19,#13#10 birthday:#341988-10-28#34#13#10}";Params="[FULLWIDTH]"
Control="Button";Name="btnRead";Caption="Click to start:";Value="Read JSON"
[/SettingsPanel]*)

var
  S: String;
  jo: TJSONObject;
begin
  if CurAction = 'btnRead' then
  begin
    S := GetParamValue('memJson');
    if S = '' then
      CurOut.Add('Json input is empty')
    else
    begin
      jo := TJSONObject.CreateWithStr(S);
      jo.putString('addName', 'Tom');
      CurOut.Add('Json read successfully');
      CurOut.Add('Read birthday: '+jo.optString('birthday'));
      CurOut.Add('Json string: ');
      CurOut.Add(jo.toString);
      CurOut.Add('');
      CurOut.Add('Json string formated: ');
      CurOut.Add(jo.toStringEx(2,0));
    end;
    Exit;
  end;
  
  if CurTable = nil then
    S:='No table selected'
  else
    S := CtObjToJsonStr(CurTable);
  CurOut.Add(S);
end.