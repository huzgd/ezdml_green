Table
--------------------------
Id            PKInteger   //[DefSelected]
Pid           Integer
Rid           FKInteger   //[DefSelected]
Name          String
Caption       String      //[DefSelected]
Memo          String      //[DefSelected]
TypeName      String
OrgId         Integer     //[DefSelected]
Period        String
CreatorId     Integer     //<<NormalIndex,NotNull>>Current user ID[DefSelected]
CreatorName   String
CreateDate    Date        //<<Default:sysdate>>Current time[DefSelected]
ModifierId    Integer
ModifierName  String
ModifyDate    Date
VersionNo     Integer
HistoryId     Integer
LockStamp     String
InsNo         Integer
ProcID        Integer
URL           String
DataLevel     Enum        //[DefSelected]
DataStatus    Enum
OrderNo       Float
TestField1    String(100) //for test
TestField2    Float(6,2)  //test selected[DefSelected]
