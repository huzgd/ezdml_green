(*
  This sample create a form to input parameters
  本脚本演示如何创建窗体输入参数
*)

//The code of the form is written to an include-file 
//这里将窗体代码写成一个函数，并写成了一个独立文件包含进来
{$INCLUDE FormTest.ps}


var
  s: String;
begin
  if IsEnglish then
  begin
    CurOut.Add('This sample shows how to create a form to gather information.');
    CurOut.Add('The code has been commented out by default to prevent frequently popup, please modify the script to see result.');
  end
  else
  begin
    CurOut.Add('本脚本演示如何创建窗体输入参数');
    CurOut.Add('为防止窗体频繁弹出，显示窗体的关键代码默认已经注掉了，请使用编辑器修改脚本以显示窗体');
    CurOut.Add('用脚本创建窗体非常麻烦，简单的还好，复杂的建议还是写个DLL来处理');
  end;
  s := '';
  //启用此行代码即可显示窗体
  //s := ShowTestForm; // <-- enable code here
  if s<>'' then
    alert(s);
end.
