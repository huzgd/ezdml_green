<%
//本脚本演示如何用JS模板生成python脚本并执行（可以理解为用JSP生成了py脚本）
/*
Note: The contents of this file will be directly overwritten when EZDML is upgraded. Instead of modifying this file directly, copy the file to a new name and modify the new file.

注意：程序升级时会直接覆盖此文件内容，请不要直接修改此文件，如果需要请将此文件复制到新名称，然后对新文件进行修改。
*/

if(!curTable){
  curOut.Add('请选择一个表对象');
  AbortOut();
}

var fJson=GetAppTempFileName("pyInput.json");//保存JSON临时文件名，传给python的，注意扩展名必须是json
curTable.saveToFile(fJson);  //保存JSON

var fOut=GetAppTempFileName("pyResOut.txt"); //结果文件名，从python传回结果用的
if(FileExists(fOut)) //删除旧的临时文件
  DeleteFile(fOut);
fOut=GetUnusedFileName(fOut);//确保结果文件名不存在（删除可能会失败的）

//下面为python脚本内容，中间混杂了EZDML的模板脚本
%>#coding=utf-8

import json
import sys
import time


#字段类型定义
dataTypeName=['Unknow','String','Integer','Float','Date','Bool','Enum','Blob','Object','Calculate','List','Function','Event','Other']

#输入JSON文件名（可能有反斜杠，需要转义成代码）
fi=${CodeTextForLang(fJson,"C")};

#打开JSON文件读取内容
print("input file: "+fi)
f = open(fi,"r",encoding='utf-8')
data = f.read()
f.close()
dm=json.loads(data)

'''
EZDML JSON格式
{
  "RootName": "TCtMetaTable",
  "Name": "member",
  "Caption": "会员",
  "MetaFields": {
    "Count": 32,
    "items": [
      {
        "ID": 26,
        "Name": "id",
        "DisplayName": "ID",
        "DataType": 2,
        "KeyFieldType": 1,
        "RelateTable": "users",
        "RelateField": "id",
        "Not_Nullable": true,
      },...
    ]
  }
}
'''

#输出测试信息
tout=['以下内容由python生成：ezdml py2 ' + time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())]

#通过JSON输出表名和字段列表
tout.append('表名：'+dm['Name']+' '+dm.get('Caption','')) 
mfs=dm['MetaFields'];
fs=mfs['items'];
i=1;
for f in fs:
    tout.append(' 字段'+str(i)+': '+f['Name']+' '+f.get('DisplayName','')+' 类型: '+dataTypeName[f['DataType']]);
    i=i+1

#当然，直接JS输出也是可以的，就是会生成重复代码
tout.append('---------------');
tout.append('直接JS输出表名：${curTable.name} ${curTable.caption}')
<%
for (var idx=0; idx<curTable.metaFields.count; idx++)
{
  var fd=curTable.metaFields.getItem(idx);
  %>
tout.append('直接JS输出字段名${idx+1}：${fd.name} ${fd.displayName}') <%
}
%>

#拼接结果字符串
data="\n".join(tout)

#输出临时文件名（由JS生成并转义成代码）
fo=${CodeTextForLang(fOut,"C")};
print("output file: "+fo) #将结果写到文本文件
f = open(fo,"w",encoding='utf-8')
f.write(data)
f.close()
    
print("done!")

<%

//输出结果是个python脚本，将其保存到py文件并调用python运行
var pyfn=GetAppTempFileName("pytest2.py");
curOut.saveToFile(pyfn);
//最后运行并返回结果，注掉这两行则保留输出为python脚本
RunCmd('python '+pyfn,-3);
curOut.loadFromFile(fOut);

%>

