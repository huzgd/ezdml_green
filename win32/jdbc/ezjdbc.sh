#!/bin/bash

java_bin=java

basepath=$(cd `dirname $0`; pwd)
source $basepath/conf.sh

jar_path="./classes"
for f in $basepath/lib/*.jar
do
jar_path=$jar_path:$f
done

export CLASSPATH=$jar_path
echo $jar_path

echo EzdmlJdbcHttpServer parameters:
echo jdbc.driver=$jdbc_driver
echo jdbc.url=$jdbc_url
echo jdbc.username=$jdbc_username
echo http.port=$http_port
echo CLASSPATH=$CLASSPATH
echo edit conf.sh to change parameters
echo -----------------------
echo starting jvm...
$java_bin com.ezdml.httpsv.EzHttpServer jdbc.driver=$jdbc_driver jdbc.url=$jdbc_url jdbc.username=$jdbc_username jdbc.password=$jdbc_password jdbc.engineType=$jdbc_engineType http.password=$http_password http.port=$http_port
echo ......
sleep 6s