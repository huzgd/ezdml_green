@echo off

%~d0
cd %~sdp0

set java_bin=java.exe

call conf.bat

setlocal enabledelayedexpansion
set LIBJARS=
for %%n in (.\lib\*.jar) do (
  set LIBJARS=!LIBJARS!%%n;
)
set CLASSPATH=%LIBJARS%

echo EzdmlJdbcHttpServer parameters:
echo jdbc.driver=%jdbc_driver%
echo jdbc.url=%jdbc_url%
echo jdbc.username=%jdbc_username%
echo http.port=%http_port%
echo CLASSPATH=%CLASSPATH%
echo edit conf.bat to change parameters
echo -----------------------
echo starting jvm...

%java_bin% com.ezdml.httpsv.EzHttpServer jdbc.driver=%jdbc_driver% jdbc.url=%jdbc_url% jdbc.username=%jdbc_username% jdbc.password=%jdbc_password% jdbc.engineType=%jdbc_engineType% http.password=%http_password% http.port=%http_port%

echo ......
choice /t 6 /d y /n >nul