#!/bin/bash

java_bin=java

basepath=$(cd `dirname $0`; pwd)

jar_path="./classes"
for f in $basepath/lib/*.jar
do
jar_path=$jar_path:$f
done

export CLASSPATH=$jar_path
echo $jar_path

echo EzdmlJdbcHttpServer parameters:
echo CLASSPATH=$CLASSPATH
echo -----------------------
echo starting jvm...
$java_bin com.ezdml.httpsv.EzHttpServer jdbc.config=jdbc_cust.properties
echo ......
sleep 6s