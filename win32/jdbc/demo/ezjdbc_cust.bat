@echo off

%~d0
cd %~sdp0

set java_bin=java.exe

setlocal enabledelayedexpansion
set LIBJARS=
for %%n in (.\lib\*.jar) do (
  set LIBJARS=!LIBJARS!%%n;
)
set CLASSPATH=%LIBJARS%

echo EzdmlJdbcHttpServer parameters:
echo CLASSPATH=%CLASSPATH%
echo -----------------------
echo starting jvm...

%java_bin% com.ezdml.httpsv.EzHttpServer jdbc.config=jdbc_cust.properties
echo ......
choice /t 6 /d y /n >nul